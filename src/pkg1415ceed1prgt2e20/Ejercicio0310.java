/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt2e20;

/**
 * Fichero: Ejercicio0310.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-oct-2013
 */
public class Ejercicio0310 {

  double p; //almacena el peso en Kg

  Ejercicio0310(double dat, String tipo) {

    p = dat;

    if ("Lb".equals(tipo)) {
      p = p / getLibras();
    }
    if ("Li".equals(tipo)) {
      p = p / getLingotes();
    }
    if ("Oz".equals(tipo)) {
      p = p / getOnzas();
    }
    if ("P".equals(tipo)) {
      p = p / getPeniques();
    }
    if ("G".equals(tipo)) {
      p = p / 1000;
    }
    if ("Q".equals(tipo)) {
      p = p / getQuintales();
    }
  }

  private double getLibras() {
    return p / 0.453;
  }

  ;
  private double getLingotes() {
    return p / 14.59;
  }

  ;
  private double getOnzas() {
    return p / 0.02835;
  }

  ;
  private double getPeniques() {
    return p / 0.00155;
  }

  ;
  private double getQuintales() {
    return p / 43.3;
  }

  ;
  public double getPeso(String tipo) {
    if ("Lb".equals(tipo)) {
      return getLibras();
    }
    if ("Li".equals(tipo)) {
      return getLingotes();
    }
    if ("Oz".equals(tipo)) {
      return getOnzas();
    }
    if ("P".equals(tipo)) {
      return getPeniques();
    }
    if ("K".equals(tipo)) {
      return p;
    }
    if ("G".equals(tipo)) {
      return p / 1000;
    }
    if ("Q".equals(tipo)) {
      return getQuintales();
    }
    return 0;
  }

  ;
  public static void main(String[] args) {
    Ejercicio0310 c = new Ejercicio0310(1000, "G");
    System.out.println(c.getPeso("K"));
    System.out.println(c.getLibras());
  }
}
/* EJECUCION:
 1.0
 2.2075055187637966
 */
