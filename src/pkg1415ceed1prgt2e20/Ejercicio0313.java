/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt2e20;

/**
 * Fichero: Ejercicio0313.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-oct-2013
 */
public class Ejercicio0313 {

  double kms;
  double litros;
  double vmed;
  double pgas;

  Ejercicio0313() {
    kms = litros = vmed = 100;
    pgas = 1.50;
  }

  public double getTiempo() { //devuelve el tiempo en horas
    return kms / vmed;
  }

  public double consumoMedio() { //litros cada 100 kms
    return (litros * 100) / kms;
  }

  public double consumoEuros() {
    return litros * pgas;
  }

  public String show() {
    return "kms: " + kms + " l:" + litros + " v:" + vmed + " consumo: " + pgas;
  }

  public static void main(String[] args) {
    Ejercicio0313 c = new Ejercicio0313();
    System.out.println(c.show());
  }
}

/* EJECUCION:
 kms: 100.0 l:100.0 v:100.0 consumo: 1.5
 */
