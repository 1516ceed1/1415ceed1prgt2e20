/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt2e20;

/**
 * Fichero: Ejercicio0311.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-oct-2013
 */
public class Ejercicio0311 {

  public double millasAMetros(int millas) {
    return 1852 * millas;
  }

  public double millasAKilometros(int millas) {
    return 1.852 * millas;
  }

  public static void main(String[] args) {
    Ejercicio0311 m = new Ejercicio0311();
    System.out.println(m.millasAKilometros(1));
    System.out.println(m.millasAMetros(1));
  }
}
/* EJECUCION:
1.852
1852.0
*/
