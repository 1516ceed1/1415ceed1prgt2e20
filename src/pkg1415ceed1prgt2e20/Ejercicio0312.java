/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt2e20;

/**
 * Fichero: Ejercicio0312.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-oct-2013
 */
public class Ejercicio0312 {

  String marca;
  String modelo;

  Ejercicio0312() {
    marca = "Seat";
    modelo = "Ibiza";
  }

  Ejercicio0312(String mar, String mod) {
    marca = mar;
    modelo = mod;
  }

  public String show() {
    return marca + " " + modelo;
  }

  public static void main(String[] args) {
    Ejercicio0312 c1 = new Ejercicio0312();
    Ejercicio0312 c2 = new Ejercicio0312("Volvo", "V60");
    System.out.println(c1.show());
    System.out.println(c2.show());
  }
}
/* EJECUCION:
Seat Ibiza
Volvo V60
*/
