/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt2e20;

/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-oct-2013
 */
class Pajaro14 {
//*** atributos o propiedades ****

  private char color; //propiedad o atributo color
  private int edad; //propiedad o atributo edad
  public int miatributo;

//*** metodos de la clase ****
  public void printMiatributo() {
    System.out.println(miatributo);
  }

  public void setedad(int e) {
    edad = e;
  }

  public void printedad() {
    System.out.println(edad);
  }

  public void setcolor(char c) {
    color = c;
  }

  public void printcolor() {
    switch (color) {
//Los pajaros son verdes, amarillos, grises, negros o blancos
//No existen pájaros de otros colores
      case 'v':
        System.out.println("verde");
        break;
      case 'a':
        System.out.println("amarillo");
        break;
      case 'g':
        System.out.println("gris");
        break;
      case 'n':
        System.out.println("negro");
        break;
      case 'b':
        System.out.println("blanco");
        break;
      default:
        System.out.println("color no establecido");
    }
  }
}

class Pajaro13 {

  public static void main(String[] args) {
    Pajaro14 p = new Pajaro14();
    //p.edad=5; // Error por ser private;
    p.miatributo = 1; // No cumple la encapsulación.
    p.setedad(5);
    p.printedad();
    p.printMiatributo();

  }
}
