
/*

 APARTADO A)

 No podemos poner las dos clases dentro del mismo fichero.
 Ya que la clase Pajaro11 que tiene el main() debe llamerse el fichero 
 igual que la clase.

 La clase Pajaro110, no puede tomar este nombre ya que existe otra clase
 con el mismo nombre en proyecto. Por eso le llamamos Pajaro20.

 B)
 Se genera un .clase por cada clase, es decir, dos .class.

 */
/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class Pajaro20 {
//*** atributos o propiedades ****

  private char color; //propiedad o atributo color
  private int edad; //propiedad o atributo edad
//*** metodos de la clase ****

  public void setedad(int e) {
    edad = e;
  }

  public void printedad() {
    System.out.println(edad);
  }

  public void setcolor(char c) {
    color = c;
  }

  public void printcolor() {
    switch (color) {
//Los pajaros son verdes, amarillos, grises, negros o blancos
//No existen pájaros de otros colores
      case 'v':
        System.out.println("verde");
        break;
      case 'a':
        System.out.println("amarillo");
        break;
      case 'g':
        System.out.println("gris");
        break;
      case 'n':
        System.out.println("negro");
        break;
      case 'b':
        System.out.println("blanco");
        break;
      default:
        System.out.println("color no establecido");
    }
  }
}

class Pajaro12 {

  public static void main(String[] args) {
    Pajaro20 p = new Pajaro20();
    // p.edad=5; // Error;
    p.setedad(5);
    p.printedad();
  }
}
