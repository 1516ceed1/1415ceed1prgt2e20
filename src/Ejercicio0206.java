

/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejercicio0206 {

  double cambio = 1.36;

  Ejercicio0206() {
    cambio = 1.36;
  }

  Ejercicio0206(double c) {
    cambio = c;
  }

  public double dolaresToEuros(double dol) {
    return dol / cambio;
  }

  public double eurosToDolares(double eur) {
    return eur * cambio;
  }

  public static void main(String[] args) {
    Ejercicio0206 f = new Ejercicio0206(1.36);
    double misdol = 50;
    double miseur = f.dolaresToEuros(misdol);
    System.out.println(misdol);
    System.out.println(miseur);
    System.out.println(f.eurosToDolares(miseur));
  }
}

/* EJECUCION:
 50.0
 36.76470588235294
 50.00000000000001
 */
