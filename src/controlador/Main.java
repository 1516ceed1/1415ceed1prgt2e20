package controlador;

import java.io.IOException;

import vista.*;
import modelo.*;

/**
 * Práctica Obligatoria Tema 3
 * @author Paco Aldarias Raya
 * @date 18/10/2013
 */
public class Main {

  public static void main(String[] args) throws IOException {

    /*
     Clase Main
     Se encargará del control del programa
     Estará la función principal
     Importará las clases de los paquetes de VISTA y MODELO
	
     */

    Persona p1;           // Declaro un objeto de la clase Persona
    p1 = new Persona();   // Construyo el objeto

    Persona p2;
    p2 = new Persona("Juan", 15);

    Vista vista;
    vista = new Vista();
    Persona p3;
    p3 = new Persona();
    p3 = vista.tomaDatos();
    vista.muestraDatos(p1, p2, p3);

  }
}
