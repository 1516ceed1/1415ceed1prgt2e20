
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejercicio0204 {

  public static void main(String[] args) {
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    int numero;

    try {
      System.out.print("Introduce numero: ");
      numero = (int) buffer.read() - '0';
      System.out.println("Numero leido: " + numero);
    } catch (Exception e) {
    }
  }

}
/* EJECUCION:
Introduce numero: 2
Numero leido: 2
*/
