

/**
 * Fichero: Ejercicio0311.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-oct-2013
 */
public class Ejercicio0211 {

  public double millasAMetros(int millas) {
    return 1852 * millas;
  }

  public double millasAKilometros(int millas) {
    return 1.852 * millas;
  }

  public static void main(String[] args) {
    Ejercicio0211 m = new Ejercicio0211();
    System.out.println(m.millasAKilometros(1));
    System.out.println(m.millasAMetros(1));
  }
}
/* EJECUCION:
1.852
1852.0
*/
