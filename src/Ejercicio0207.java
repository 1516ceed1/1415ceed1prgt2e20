

/**
 * Fichero: Ejercicio0307.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-oct-2013
 */
public class Ejercicio0207 {

  int dato;

  Ejercicio0207(int d) {
    dato = d;
  }

  public int doble() {
    return dato + dato;
  }

  public int triple() {
    return doble() + dato;
  }

  public int cuadruple() {
    return triple() + dato;
  }

  public void printDato(int n) {
    System.out.println(n);
  }

  public static void main(String[] args) {

    Ejercicio0207 m = new Ejercicio0207(5);
    System.out.println(m.doble());
    System.out.println(m.triple());
    System.out.println(m.cuadruple());

  }
}
/* EJECUCION:
 10
 15
 20
 */
